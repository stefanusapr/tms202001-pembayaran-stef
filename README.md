# Aplikasi Pembayaran #

Menjalankan aplikasi :

1. Buat user database

    ```
    createuser -P namauser
    ```

2. Buat database

    ```
    createdb -Onamauser namadatabase
    ```

3. Jalankan aplikasi

   ```
   mvn clean spring-boot:run
   ```